# Task Assignment - Web Developer (Creative Handles) by Hasanta Sumudupriya

## Steps to run the HTML application

1. Clone this git repository into your computer.
1. Run `npm install` to install node module dependancies.
1. Open `index.html` file inside the `public` folder using your web browser.

### That's it. Enjoy the HTML application. Thank you!
